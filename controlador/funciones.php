<?php

include_once ('../modelo/modeloFichero.php');
include_once ('../modelo/modeloSQL.php');
//include_once ('../vista/Config.php');

function recoge($valor) {
    $resultado = "";
    
    if (isset($_REQUEST[$valor])) {
        $resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    }
    
    return $resultado;
}


function setModelo($modelo,$nombreFichero) {
        
        $file = fopen($nombreFichero, "w");
        fwrite($file, $modelo);
        fclose($file);
    }
    
    function getModelo(){
        
        $modelo = "";
        $fp = fopen("../ficheros/modelo.txt", "r");
        $linea = fgets($fp);
        fclose($fp);
       
        if($linea == "Sql"){
            $modelo = new ModeloSQL();
        }else if($linea == "Ficheros"){
            $modelo = new ModeloFichero();
        }
        
        return $modelo;
    }
    
    function getVista(){
        
        $fp = fopen("../ficheros/modelo.txt", "r");
        $linea = fgets($fp);
        fclose($fp);
        return $linea;
    }
        
        
    
    
 
